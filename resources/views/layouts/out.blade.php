<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">

        <title>Pembayaran Listrik</title>
        <!--===============================================================================================-->
        <link rel="icon" type="image/png" href="{{url('listrik.png')}}"/>
        <!--===============================================================================================-->

        <!-- General CSS Files -->
        <link rel="stylesheet" type="text/css" href="{{url('out/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('out/css/all.min.css')}}">

        <!-- Template CSS -->
        <link rel="stylesheet" type="text/css" href="{{url('out/css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('out/css/components.css')}}">
        <!--===============================================================================================-->
        <style type="text/css">
            body {
                background: linear-gradient(90deg, rgba(217,209,65,1) 0%, rgba(250,51,103,1) 49%, rgba(139,0,255,1) 100%);
                background-size: 400% 400%;
                animation: gradient 15s ease infinite;
                height: 100vh;
            }

            @keyframes gradient {
                0% {
                    background-position: 0% 50%;
                }
                50% {
                    background-position: 100% 50%;
                }
                100% {
                    background-position: 0% 50%;
                }
            }

        </style>
    </head>

    <body>
        <div id="app">
            <section class="section">

                @yield('content')

            </section>
        </div>
        <!-- General JS Scripts -->
        <script src="{{url('out/js/jquery.min.js')}}"></script>
        <script src="{{url('out/js/popper.js')}}"></script>
        <script src="{{url('out/js/tooltip.js')}}"></script>
        <script src="{{url('out/js/bootstrap.min.js')}}"></script>
        <script src="{{url('out/js/jquery.nicescroll.min.js')}}"></script>
        <script src="{{url('out/js/moment.min.js')}}"></script>
        <script src="{{url('out/js/stisla.js')}}"></script>
      
        <!-- JS Libraies -->

        <!-- Page Specific JS File -->
      
        <!-- Template JS File -->
        <script src="{{url('out/js/scripts.js')}}"></script>
        <script src="{{url('out/js/custom.js')}}"></script>
    </body>
</html>

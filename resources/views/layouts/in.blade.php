
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Pembayaran Listrik</title>
    <link rel="icon" type="image/png" href="{{url('listrik.png')}}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- General CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{url('out/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('out/css/all.min.css')}}">

    <!-- Template CSS -->
    <link rel="stylesheet" type="text/css" href="{{url('out/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('out/css/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('out/css/sweetalert2/sweetalert2.min.css')}}" />
    @stack('script-header')
</head>

<body>
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            <div class="navbar-bg"></div>
            <nav class="navbar navbar-expand-lg main-navbar">
                <ul class="navbar-nav mr-3 mr-auto">
                    <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
                </ul>
                <ul class="navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                            @if(Auth::user()->photo != null)
                                <img alt="image" src="{{url(Auth::user()->photo)}}" class="rounded-circle mr-1">
                            @else
                                <img alt="image" src="{{url('images\user.png')}}" class="rounded-circle mr-1">
                            @endif
                            
                            <div class="d-sm-none d-lg-inline-block">Hi, {{Auth::User()->nama}}</div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="dropdown-title">
                                @if(Auth::user()->last_login_time != null)
                                    Login sejak {{ auth()->user()->last_login_time->diffForHumans() }}
                                @else
                                    {{$waktu}}
                                @endif
                            </div>
                            <a href="{{ route('profile') }}" class="dropdown-item has-icon">
                                <i class="far fa-user"></i> Profile
                            </a>
                            <div class="dropdown-divider"></div>
                                <a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger">
                                <i class="fas fa-sign-out-alt"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </nav>

            <div class="main-sidebar sidebar-style-2">
                <aside id="sidebar-wrapper">
                    <div class="sidebar-brand">
                        <a href="{{url('/home')}}">Pembayaran Listrik</a>
                    </div>
                    <div class="sidebar-brand sidebar-brand-sm">
                        <a href="{{url('/home')}}">IMI</a>
                    </div>

                    <ul class="sidebar-menu">
                        <li class="menu-header">Dashboard</li>
                        <li><a class="nav-link" href="{{url('/home')}}"><i class="fas fa-fire" style="color: red"></i> <span>Dashboard</span></a></li>
                        <li><a class="nav-link" href="{{url('/administratif/pelanggan')}}"><i class="fas fa-users" style="color: blue"></i> <span>Pelanggan</span></a></li>
                        <li><a class="nav-link" href="{{url('/administratif/tarif')}}"><i class="fas fa-dollar-sign" style="color: green"></i> <span>Tarif</span></a></li>
                        <li><a class="nav-link" href="{{url('/administratif/penggunaan')}}"><i class="fas fa-tachometer-alt" style="color: olive"></i> <span>Penggunaan</span></a></li>
                        <li><a class="nav-link" href="{{url('/administratif/tagihan')}}"><i class="fas fa-receipt" style="color: maroon"></i> <span>Tagihan</span></a></li>
                        <li><a class="nav-link" href="{{url('/administratif/pembayaran')}}"><i class="fas fa-hand-holding-usd" style="color: teal"></i> <span>Pembayaran</span></a></li>
                        <li><a class="nav-link" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt" style="color: red"></i> <span>Logout</span></a></li>
                    </ul>
                    <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
                        <a href="{{url('/administratif/algoritma')}}" class="btn btn-primary btn-lg btn-block btn-icon-split">
                            <i class="fas fa-rocket"></i> Algoritma
                        </a>
                    </div>
                </aside>
            </div>

            <!-- Main Content -->
            <div class="main-content">
                <section class="section">
          
                    @yield('content')

                </section>
             </div>
            <footer class="main-footer">
                <div class="footer-left">
                    Copyright &copy; 2022 <div class="bullet"></div> Design For <a href="#">Sertifikasi Universitas Nusa Mandiri</a>
                </div>
                <div class="footer-right">
          
                </div>
            </footer>
        </div>
    </div>

    <!-- General JS Scripts -->
    <script src="{{url('out/js/jquery.min.js')}}"></script>
    <script src="{{url('out/js/popper.js')}}"></script>
    <script src="{{url('out/js/tooltip.js')}}"></script>
    <script src="{{url('out/js/bootstrap.min.js')}}"></script>
    <script src="{{url('out/js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{url('out/js/moment.min.js')}}"></script>
    <script src="{{url('out/js/stisla.js')}}"></script>



    <!-- Template JS File -->
    <script src="{{url('out/js/scripts.js')}}"></script>
    <script src="{{url('out/js/custom.js')}}"></script>
    <script src="{{url('out/css/sweetalert2/sweetalert2.min.js')}}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'X-Requested-With': 'XMLHttpRequest',
            }
        })
    </script>
    @stack('script-footer')
</body>
</html>
@extends('layouts.in')

@section('content')
<div class="section-header">
    <h1>Dashboard</h1>
</div>
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                Selamat datang, {{Auth::User()->nama}}!
            </div>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-primary">
                <i class="fas fa-users"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>Pelanggan</h4>
                </div>
                <div class="card-body">
                    {{$countuser}}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-warning">
                <i class="far fa-newspaper"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>Tagihan</h4>
                </div>
                <div class="card-body">
                    {{$counttagihan}}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
                <i class="fas fa-calendar-times"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>Belum Bayar</h4>
                </div>
                <div class="card-body">
                    {{$countbelumbayar}}
                </div>
            </div>
        </div> 
    </div>  
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-success">
                <i class="fas fa-calendar-check"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>Terbayar</h4>
                </div>
                <div class="card-body">
                    {{$countterbayar}}
                </div>
            </div>
        </div> 
    </div>    
</div>

<div class="container">
    
</div>
@endsection

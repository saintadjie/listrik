$(function () {
    $('.datepickerm').datepicker({
        format: 'mm',
        viewMode : 'months',
        minViewMode : 'months',
        autoclose: true,
        language: 'id',
    });

    $('.datepicker').datepicker({
        format: 'yyyy',
        viewMode : 'years',
        minViewMode : 'years',
        autoclose: true,
    });

	$('#btn_simpan').click(function(){
        if ($('#username').val() == null) {
            Swal.fire( "Kesalahan", "Pelanggan harus dipilih", "error" )
            return
        }
        else if ($('#bulan').val() == '') {
            Swal.fire( "Kesalahan", "Kolom bulan tidak boleh kosong", "error" )
            return
        }
        else if ($('#tahun').val() == '') {
            Swal.fire( "Kesalahan", "Kolom tahun tidak boleh kosong", "error" )
            return
        }
        else if ($('#meter_awal').val() == '') {
            Swal.fire( "Kesalahan", "Kolom meter awal tidak boleh kosong", "error" )
            return
        }
        else if ($('#meter_akhir').val() == '') {
            Swal.fire( "Kesalahan", "Kolom meter akhir tidak boleh kosong", "error" )
            return
        }

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "ID Penggunaan : "+$('#id_penggunaanlama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "PUT",
                    url: url_api,
                    data: {
                        id_penggunaan   : $("#id_penggunaan").val(),
                        id_tagihan      : $("#id_tagihan").val(),
                        username        : $("#username").val(),
                        bulan           : $("#bulan").val(),
                        tahun           : $("#tahun").val(),
                        meter_awal      : $("#meter_awal").val(),
                        meter_akhir     : $("#meter_akhir").val(),
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_main
                            });
                        } else {
                            Swal.fire("Kesalahan", "Data gagal diubah", "error")
                        }   
                    }
                });
            }
        })
        
    })

})
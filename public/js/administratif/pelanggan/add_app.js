$(function () {

    $('#btn_reset').click(function(){
        $('#username').val('')
        $('#nama').val('')
        $('#password').val('')
        $('#konfirmasi_password').val('')
        $('#alamat').val('')
        $('#id_tarif').val('pilih').trigger('change');
    });

    $('#btn_simpan').click(function(){

        if ($('#username').val() == '') {
            Swal.fire( "Kesalahan", "Username tidak boleh kosong", "error" )
            return
        } else if ($('#nama').val() == '') {
            Swal.fire( "Kesalahan", "Nama tidak boleh kosong", "error" )
            return
        } else if ($('#password').val() == '') {
            Swal.fire( "Kesalahan", "Password tidak boleh kosong", "error" )
            return
        }  else if ($('#konfirmasi_password').val() == '') {
            Swal.fire("Kesalahan", "Konfirmasi Password tidak boleh kosong", "error");
            return false
        } else if ($('#password').val() != $('#konfirmasi_password').val()) {
            Swal.fire("Kesalahan", "Password dan Konfirmasi Password tidak cocok", "error");
            return false
        } else if ($('#alamat').val() == '') {
            Swal.fire( "Kesalahan", "Alamat tidak boleh kosong", "error" )
            return
        } else if ($('#id_tarif')[0].selectedIndex <= 0) {
            Swal.fire( "Kesalahan", "ID Tarif harus dipilih", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api,
            data: {
                username    : $("#username").val(),
                nama        : $("#nama").val(),
                password    : $("#password").val(),
                alamat      : $("#alamat").val(),
                id_tarif    : $("#id_tarif").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        title: "Data telah disimpan",
                        timer: 3000,
                        showConfirmButton: true,
                        html: 'Otomatis tertutup dalam <b></b> milidetik.',
                        timerProgressBar: true,
                        onBeforeOpen: () => {
                            Swal.showLoading()
                            timerInterval = setInterval(() => {
                              const content = Swal.getContent()
                              if (content) {
                                const b = content.querySelector('b')
                                if (b) {
                                  b.textContent = Swal.getTimerLeft()
                                }
                              }
                            }, 100)
                        },
                        onClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then(function (result) {
                        $('#username').val('')
                        $('#nama').val('')
                        $('#password').val('')
                        $('#konfirmasi_password').val('')
                        $('#alamat').val('')
                        $('#id_tarif').val('pilih').trigger('change');
                    })
                } else if(data.status=='FAILED'){
                    Swal.fire("Kesalahan", "Username telah digunakan", "error");
                } else {
                    Swal.fire("Kesalahan", "Data gagal disimpan", "error")
                } 
            }
        })
    })

})

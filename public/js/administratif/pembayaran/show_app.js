$(function () {
    $('.datepickerm').datepicker({
        format: 'mm',
        viewMode : 'months',
        minViewMode : 'months',
        autoclose: true,
        language: 'id',
    });

    $('.datepicker').datepicker({
        format: 'yyyy',
        viewMode : 'years',
        minViewMode : 'years',
        autoclose: true,
    });


    $('#id_pembayaran').val('PB' + moment().format('YYYY') + moment().format('MMDD') + moment().format('hhmmss'));

	$('#btn_simpan').click(function(){
        if ($('#biaya_admin').val() == '') {
            Swal.fire( "Kesalahan", "Kolom biaya admin tidak boleh kosong", "error" )
            return
        }
        else if ($('#biaya_admin').val() < 0) {
            Swal.fire( "Kesalahan", "Kolom biaya admin tidak boleh kurang dari 0", "error" )
            return
        }

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Pembayaran dengan ID tagihan : "+$('#id_tagihan').val()+" akan disimpan?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "PUT",
                    url: url_api,
                    data: {
                        tanggal_pembayaran  : $("#tanggal_pembayaran").val(),
                        id_pembayaran       : $("#id_pembayaran").val(),
                        id_tagihan          : $("#id_tagihan").val(),
                        id_penggunaan       : $("#id_penggunaan").val(),
                        username            : $("#username").val(),
                        bulan               : $("#bulan").val(),
                        tahun               : $("#tahun").val(),
                        jumlah_meter        : $("#jumlah_meter").val(),
                        biaya_admin         : $("#biaya_admin").val(),
                        total_bayar         : $("#total_bayar").val(),
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah disimpan",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_main
                            });
                        } else {
                            Swal.fire("Kesalahan", "Data gagal diubah", "error")
                        }   
                    }
                });
            }
        })
        
    })

})
<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // data faker indonesia
        $faker = Faker::create('id_ID');
 
        // membuat data dummy sebanyak 30 record
        for($x = 1; $x <= 500; $x++){
 
        	// insert data dummy pegawai dengan faker
        	DB::table('ms_angkatan_pegawai')->insert([
        		'kode_angkatan_pegawai' => $faker->unique()->numerify('##################'),
		        'nama_angkatan_pegawai' => $faker->name,
		        'tahun_angkatan_pegawai' => $faker->date($format = 'Y', $max = 'now'),
		        'level_angkatan_pegawai' => $faker->randomNumber(2),

        	]);

        	// insert data dummy hari libur dengan faker
        	DB::table('ms_hari_libur')->insert([
        		'kode_hari_libur' => $faker->unique()->numerify('##################'),
		        'tanggal_hari_libur' => $faker->date($format = 'Y-m-d', $max = 'now'),
		        'keterangan_hari_libur' => $faker->jobTitle,

        	]);

        	// insert data dummy hubungan keluarga dengan faker
        	DB::table('ms_hubungan_keluarga')->insert([
        		'kode_hubungan_keluarga' => $faker->unique()->numerify('##################'),
		        'jenis_hubungan_keluarga' => $faker->jobTitle,

        	]);

            // insert data dummy jenis diklat dengan faker
            DB::table('ms_jenis_diklat')->insert([
                'kode_jenis_diklat' => $faker->unique()->numerify('##################'),
                'nama_jenis_diklat' => $faker->name,
                'kesetaraan_jenis_diklat' => $faker->name,

            ]);

            // insert data dummy jenis hukuman dengan faker
            DB::table('ms_jenis_hukuman')->insert([
                'kode_jenis_hukuman' => $faker->unique()->numerify('##################'),
                'nama_jenis_hukuman' => $faker->name,
                'skala_jenis_hukuman' => $faker->name,

            ]);

            // insert data dummy jenis pendidikan formal dengan faker
            DB::table('ms_jenis_pendidikan_formal')->insert([
                'kode_jenis_pendidikan_formal' => $faker->unique()->numerify('##################'),
                'nama_jenis_pendidikan_formal' => $faker->name,

            ]);

            // insert data dummy jurusan pendidikan formal dengan faker
            DB::table('ms_jurusan_pendidikan_formal')->insert([
                'kode_jurusan_pendidikan_f' => $faker->unique()->numerify('##################'),
                'nama_jurusan_pendidikan_f' => $faker->name,

            ]);

            // insert data dummy pendidikan dan pelatihan lain dengan faker
            DB::table('ms_pendidikan_dan_pelatihan_lain')->insert([
                'kode_pdp_lain' => $faker->unique()->numerify('##################'),
                'nama_pdp_lain' => $faker->name,

            ]);

            // insert data dummy pengeluaran sk hukuman dinas dengan faker
            DB::table('ms_pengeluaran_sk_hukuman_dinas')->insert([
                'kode_pengeluaran_skhukdis' => $faker->unique()->numerify('##################'),
                'nama_pengeluaran_skhukdis' => $faker->name,

            ]);

            // insert data dummy periode dp3 dengan faker
            DB::table('ms_periode_dp3')->insert([
                'kode_periode_dp3' => $faker->unique()->numerify('##################'),
                'tahun_periode_dp3' => $faker->date($format = 'Y', $max = 'now'),
                'tanggal_awal_periode_dp3' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'tanggal_akhir_periode_dp3' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'batas_waktu_periode_dp3' => $faker->date($format = 'Y-m-d', $max = 'now'),

            ]);

            // insert data dummy pegawai dengan faker
            DB::table('ms_tingkat_golongan')->insert([
                'kode_tingkat_golongan' => $faker->unique()->numerify('##################'),
                'nama_tingkat_golongan' => $faker->name,
                'golongan_tingkat_golongan' => $faker->randomElement($array = array ('I','II','III', 'IV')),
                'ruang_tingkat_golongan' => $faker->randomElement($array = array ('a','b','c', 'd')),
                'gaji_pokok_tingkat_golongan' => $faker->numberBetween($min = 10000000, $max = 100000000),

            ]);
 
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use App\Models\tarif;


class AdministratifSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$file = storage_path() . '/app/seeder/administratif.xlsx';
        $reader = ReaderFactory::create(Type::XLSX);
        
        $reader->open($file);

        foreach ($reader->getSheetIterator() as $sheet) {

            if ($sheet->getName() === 'TARIF') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'id_tarif' => $row[0],
                            'daya' => $row[1],
                            'tarifperkwh' => $row[2],
                        ];

                        $this->fillData(new tarif(), $data);
                    }
                }

            }

            

        } 

        $reader->close();

    }

    function fillData($model, $data) 
    {
        $model->fill($data);
        $model->save();
    }

}
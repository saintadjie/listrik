<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesandPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions    
        // Master Data
        Permission::create(['name' => 'Melihat daftar pelanggan']);
        Permission::create(['name' => 'Menambah data pelanggan']);
        Permission::create(['name' => 'Menghapus data pelanggan']);
        Permission::create(['name' => 'Mengubah data pelanggan']);
        Permission::create(['name' => 'Mengubah data pelanggan lain']);
        Permission::create(['name' => 'Melihat data pelanggan terhapus']);
        Permission::create(['name' => 'Mengembalikan data pelanggan terhapus']);

        Permission::create(['name' => 'Melihat daftar tarif']);
        Permission::create(['name' => 'Menambah data tarif']);
        Permission::create(['name' => 'Menghapus data tarif']);
        Permission::create(['name' => 'Mengubah data tarif']);
        Permission::create(['name' => 'Mengubah data tarif lain']);
        Permission::create(['name' => 'Melihat data tarif terhapus']);
        Permission::create(['name' => 'Mengembalikan data tarif terhapus']);

        Permission::create(['name' => 'Melihat daftar penggunaan']);
        Permission::create(['name' => 'Menambah data penggunaan']);
        Permission::create(['name' => 'Menghapus data penggunaan']);
        Permission::create(['name' => 'Mengubah data penggunaan']);
        Permission::create(['name' => 'Mengubah data penggunaan lain']);
        Permission::create(['name' => 'Melihat data penggunaan terhapus']);
        Permission::create(['name' => 'Mengembalikan data penggunaan terhapus']);

        Permission::create(['name' => 'Melihat daftar tagihan']);
        Permission::create(['name' => 'Menambah data tagihan']);
        Permission::create(['name' => 'Menghapus data tagihan']);
        Permission::create(['name' => 'Mengubah data tagihan']);
        Permission::create(['name' => 'Mengubah data tagihan lain']);
        Permission::create(['name' => 'Melihat data tagihan terhapus']);
        Permission::create(['name' => 'Mengembalikan data tagihan terhapus']);

        Permission::create(['name' => 'Melihat daftar pembayaran']);
        Permission::create(['name' => 'Menambah data pembayaran']);
        Permission::create(['name' => 'Menghapus data pembayaran']);
        Permission::create(['name' => 'Mengubah data pembayaran']);
        Permission::create(['name' => 'Mengubah data pembayaran lain']);
        Permission::create(['name' => 'Melihat data pembayaran terhapus']);
        Permission::create(['name' => 'Mengembalikan data pembayaran terhapus']);

        // create roles and assign existing permissions
        
        $role = Role::create(['name' => 'SUPERADMIN']);
        $role->givePermissionTo([	
                                    'Melihat daftar pelanggan', 
                                    'Menambah data pelanggan', 
                                    'Menghapus data pelanggan',
                                    'Mengubah data pelanggan',
                                    'Mengubah data pelanggan lain',
                                    'Mengembalikan data pelanggan terhapus',

                                    'Melihat daftar tarif', 
                                    'Menambah data tarif', 
                                    'Menghapus data tarif',
                                    'Mengubah data tarif',
                                    'Mengubah data tarif lain',
                                    'Mengembalikan data tarif terhapus',

                                    'Melihat daftar penggunaan', 
        							'Menambah data penggunaan', 
                                    'Menghapus data penggunaan',
        							'Mengubah data penggunaan',
                                    'Mengubah data penggunaan lain',
                                    'Mengembalikan data penggunaan terhapus',

                                    'Melihat daftar tagihan', 
                                    'Menambah data tagihan', 
                                    'Menghapus data tagihan',
                                    'Mengubah data tagihan',
                                    'Mengubah data tagihan lain',
                                    'Mengembalikan data tagihan terhapus',

                                    'Melihat daftar pembayaran', 
                                    'Menambah data pembayaran', 
                                    'Menghapus data pembayaran',
                                    'Mengubah data pembayaran',
                                    'Mengubah data pembayaran lain',
                                    'Mengembalikan data pembayaran terhapus'
        						]);

       	$role = Role::create(['name' => 'PELANGGAN']);
        $role->givePermissionTo([	
                                    'Melihat daftar pelanggan',
                                    'Mengubah data pelanggan',
                                    'Melihat daftar tarif',
                                    'Melihat daftar penggunaan', 
                                    'Melihat daftar tagihan',
                                    'Melihat daftar pembayaran',
        						]);
    }
}

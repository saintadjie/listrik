<?php

use Illuminate\Database\Seeder;
use App\User;

class PelangganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $data = [
            'id' => '2',
            'username' => 'pelanggan',
            'nama' => 'pelanggan',
            'password' => '$2y$10$JRpVSYEqVN7X9SsiPmFTEe9axbzijp5E1pdp1C4nGZZ6M/9fV0iI2',
            'alamat' => 'Bekasi',
            'id_tarif' => 'TRF1300',
            'level_pengguna' => '2',
        ];
        $user->fill($data);
        $user->save();
        $user->assignRole('PELANGGAN');
    }
}

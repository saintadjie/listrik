<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class tarif extends Model
{
    use SoftDeletes;
    protected $table = 'tarifs';

    protected $fillable = [
        'id_tarif',
        'daya',
        'tarifperkwh',
    ];
}

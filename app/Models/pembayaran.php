<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class pembayaran extends Model
{
    use SoftDeletes;
    protected $table = 'pembayarans';

    protected $fillable = [
        'id_pembayaran',
        'id_tagihan',
        'username',
        'tanggal_pembayaran',
        'biaya_admin',
        'total_bayar',
    ];
}
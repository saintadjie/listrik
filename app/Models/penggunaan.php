<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class penggunaan extends Model
{
    use SoftDeletes;
    protected $table = 'penggunaans';

    protected $fillable = [
        'id_penggunaan',
        'username',
        'bulan',
        'tahun',
        'meter_awal',
        'meter_akhir',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class tagihan extends Model
{
    use SoftDeletes;
    protected $table = 'tagihans';

    protected $fillable = [
        'id_tagihan',
        'id_penggunaan',
        'username',
        'bulan',
        'tahun',
        'jumlah_meter',
        'status',
    ];
}

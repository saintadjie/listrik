<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['prefix' => 'administratif', 'middleware' => 'auth'], function () {

	Route::group(['prefix' => 'algoritma'], function () {

	    Route::GET('/', 'AlgoritmaController@index');

	});

	Route::group(['prefix' => 'pelanggan'], function () {

	    Route::GET('/', 'PelangganController@index')->middleware('permission:Melihat daftar pelanggan');
	    Route::GET('add', 'PelangganController@page_add')->middleware('permission:Menambah data pelanggan');
	    Route::GET('profile', 'PelangganController@page_profile')->middleware('permission:Mengubah data pelanggan')->name('profile');
	    Route::GET('pullData', 'PelangganController@pullData')->name('pullData.pelanggan');
	    Route::GET('show/{username}', 'PelangganController@page_show')->middleware('permission:Mengubah data pelanggan lain');

	});

	Route::group(['prefix' => 'tarif'], function () {

	    Route::GET('/', 'TarifController@index')->middleware('permission:Melihat daftar tarif');
	    Route::GET('add', 'TarifController@page_add')->middleware('permission:Menambah data tarif');
	    Route::GET('pullData', 'TarifController@pullData')->name('pullData.tarif');
	    Route::GET('show/{id_tarif}', 'TarifController@page_show')->middleware('permission:Mengubah data tarif');

	});

	Route::group(['prefix' => 'penggunaan'], function () {

	    Route::GET('/', 'PenggunaanController@index')->middleware('permission:Melihat daftar penggunaan');
	    Route::GET('add', 'PenggunaanController@page_add')->middleware('permission:Menambah data penggunaan');
	    Route::GET('pullData', 'PenggunaanController@pullData')->name('pullData.penggunaan');
	    Route::GET('show/{id_penggunaan}', 'PenggunaanController@page_show')->middleware('permission:Mengubah data penggunaan');

	});

	Route::group(['prefix' => 'tagihan'], function () {

	    Route::GET('/', 'TagihanController@index')->middleware('permission:Melihat daftar tagihan');
	    Route::GET('add', 'TagihanController@page_add')->middleware('permission:Menambah data tagihan');
	    Route::GET('pullData', 'TagihanController@pullData')->name('pullData.tagihan');
	    Route::GET('show/{id_tagihan}', 'TagihanController@page_show')->middleware('permission:Mengubah data tagihan');

	});

	Route::group(['prefix' => 'pembayaran'], function () {

	    Route::GET('/', 'PembayaranController@index')->middleware('permission:Melihat daftar pembayaran');
	    Route::GET('add', 'PembayaranController@page_add')->middleware('permission:Menambah data pembayaran');
	    Route::GET('pullData', 'PembayaranController@pullData')->name('pullData.pembayaran');
	    Route::GET('show/{id_pembayaran}', 'PembayaranController@page_show')->middleware('permission:Mengubah data pembayaran');

	});

});
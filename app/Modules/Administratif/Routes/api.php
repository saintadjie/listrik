<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/administratif', 'middleware' => 'auth:api'], function () {

    Route::group(['prefix' => 'pelanggan'], function () {
        Route::DELETE('delete/{username}', 'PelangganController@delete');
        Route::PUT('edit', 'PelangganController@edit');
        Route::POST('search', 'PelangganController@search');
        Route::POST('store', 'PelangganController@store');
        Route::PUT('edit_profile', 'PelangganController@edit_profile');
        Route::PUT('edit_password', 'PelangganController@edit_password');
    });

    Route::group(['prefix' => 'tarif'], function () {
        Route::DELETE('delete/{id_tarif}', 'TarifController@delete');
        Route::PUT('edit', 'TarifController@edit');
        Route::POST('search', 'TarifController@search');
        Route::POST('store', 'TarifController@store');
    });

    Route::group(['prefix' => 'penggunaan'], function () {
        Route::DELETE('delete/{id_penggunaan}', 'PenggunaanController@delete');
        Route::PUT('edit', 'PenggunaanController@edit');
        Route::POST('search', 'PenggunaanController@search');
		Route::POST('store', 'PenggunaanController@store');
    });

    Route::group(['prefix' => 'pembayaran'], function () {
        Route::PUT('edit', 'PembayaranController@edit');
        Route::POST('search', 'PembayaranController@search');
        Route::POST('store', 'PembayaranController@store');
    });

});
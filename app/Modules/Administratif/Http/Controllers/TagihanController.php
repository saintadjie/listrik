<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\tagihan;
use Yajra\DataTables\DataTables;
use App\User;
use Auth;

class TagihanController extends Controller
{
    public function pullData(Request $request) {

        $level_pengguna = Auth::User()->level_pengguna;
        $username       = Auth::User()->username;

        if($level_pengguna == 1 ){
            return datatables (tagihan::select('id', 'id_tagihan', 'id_penggunaan', 'username', 'bulan', 'tahun', 'jumlah_meter', 'status')
            ->where('deleted_at','=', NULL)
            ->latest())
            ->addIndexColumn()
            ->toJson();

        } else {
            return datatables (tagihan::select('id', 'id_tagihan', 'id_penggunaan', 'username', 'bulan', 'tahun', 'jumlah_meter', 'status')
            ->where('deleted_at','=', NULL)
            ->where('username', '=', $username)
            ->latest())
            ->addIndexColumn()
            ->toJson();
        }

        
    }

    public function index(Request $request) {

        return view('administratif::tagihan.index');

    }

}

<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\tarif;
use Yajra\DataTables\DataTables;
use App\User;

class TarifController extends Controller
{
    public function pullData(Request $request) {

        return datatables (tarif::select('id', 'id_tarif', 'daya', 'tarifperkwh')
            ->where('deleted_at','=', NULL)
            ->latest())
            ->addIndexColumn()
            ->toJson();

    }

    public function index(Request $request) {

        return view('administratif::tarif.index');

    }

    public function page_show($id_tarif) {

        $rs = tarif::where('id_tarif', $id_tarif)->first();
        
        return view('administratif::tarif.show', ['rs' => $rs]);

    }

    public function delete($id_tarif) {

        $rs = tarif::where('id_tarif', $id_tarif)->first();
        $rs->delete();

        activity()
        ->performedOn($rs)
        ->withProperties(['id_tarif' => $rs->id_tarif, 'daya' => $rs->daya])
        ->log('Menghapus Data ID tarif '. $rs->id_tarif);

        if ($rs){

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }
        
    }

    public function page_add() {

        return view('administratif::tarif.add');

    }

    public function store(Request $request) {

        $check      = tarif::where('id_tarif', $request->id_tarif)->first();
        $check2     = tarif::onlyTrashed()->where('id_tarif', $request->id_tarif)->first();

        if (!empty($check)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
        }

        elseif (!empty($check2)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
        } 


        $rs = tarif::firstOrCreate(
            ['id_tarif' => $request->id_tarif],
            ['daya' => $request->daya, 'tarifperkwh' => $request->tarifperkwh]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['id_tarif' => $rs->id_tarif, 'daya' => $rs->daya])
            ->log('Menambah Data ID tarif ' . $rs->id_tarif);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function edit(Request $request) {


        $id_tarif        = $request->id_tarif;
        $id_tariflama    = $request->id_tariflama;

        if ($id_tariflama != $id_tarif) {
            $check      = tarif::where('id_tarif', $request->id_tarif)->first();
            $check2     = tarif::onlyTrashed()->where('id_tarif', $request->id_tarif)->first();

            if (!empty($check)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
            }
            elseif (!empty($check2)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
            }
        }

        $rs = tarif::updateOrCreate(
            ['id_tarif'    => $request->id_tariflama],
            ['id_tarif' => $request->id_tarif, 'daya' => $request->daya, 'tarifperkwh' => $request->tarifperkwh]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['id_tarif' => $rs->id_tarif, 'daya' => $rs->daya])
            ->log('Mengubah Data ID Tarif ' . $id_tariflama);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        } 
    }
}

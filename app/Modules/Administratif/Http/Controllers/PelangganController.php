<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\penggunaan;
use App\Models\tarif;
use App\Models\tagihan;
use Yajra\DataTables\DataTables;
use App\User;
use Hash;
use Auth;

class PelangganController extends Controller
{
    public function pullData(Request $request) {

        return datatables (User::select('id', 'username', 'nama', 'alamat')
            ->where('deleted_at','=', NULL)
            ->where('level_pengguna', '!=', '1')
            ->latest())
            ->addIndexColumn()
            ->toJson();

    }

    public function index(Request $request) {

        return view('administratif::pelanggan.index');

    }

    public function page_show($id_penggunaan) {

        $rs = penggunaan::where('penggunaans.id_penggunaan', $id_penggunaan)
                ->join('tagihans', 'penggunaans.id_penggunaan', 'tagihans.id_penggunaan')
                ->first();

        $rsp = User::select('username', 'nama', DB::raw("CONCAT(username, ' - ', nama) as display"))
            ->where('level_pengguna','!=','1')
            ->get();


        return view('administratif::pelanggan.show', ['rs' => $rs, 'rsp' => $rsp]);

    }

    public function delete($username) {

        $rs = user::where('username', $username)->first();
        $rs->delete();

        activity()
        ->performedOn($rs)
        ->withProperties(['username' => $rs->username, 'username' => $rs->username])
        ->log('Menghapus Data ID Penggunaan '. $rs->username);

        if ($rs){

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }
        
    }

    public function page_add() {

        $rs = tarif::select('id_tarif', 'daya', 'tarifperkwh', DB::raw("CONCAT(' Daya ', daya, ' - Harga Rp. ', tarifperkwh) as display"))
            ->get();

        return view('administratif::pelanggan.add', ['rs' => $rs]);

    }

    public function store(Request $request) {

        $check      = User::where('username', $request->username)->first();

        if (!empty($check)) {
            return response()->json( [ 'status' => 'FAILED', 'message' => 'Duplicate' ] );
        }

        $rs = User::firstOrCreate(
            ['username' => $request->username],
            ['nama' => $request->nama, 'password' => Hash::make($request->password), 'alamat' => $request->alamat, 'id_tarif' => $request->id_tarif, 'level_pengguna' => 2]
        );

        $role = 'PELANGGAN';

        $rs->assignRole($role);


        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['username' => $rs->username, 'nama' => $rs->nama])
            ->log('Menambah Data Pelanggan ' . $rs->username);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }


    }

    public function page_profile() {

        $userID             = Auth::user()->id;
        $userLevel          = Auth::user()->level_pengguna;
        $username           = Auth::user()->username;
        $counttagihan       = tagihan::where('username','=',$username)->count();
        $countbelumbayar    = tagihan::where('username','=',$username)->where('status','=','BELUM BAYAR')->count();   
        $countterbayar      = tagihan::where('username','=',$username)->where('status','=','TERBAYAR')->count();
        
        $rs                 = User::findOrfail($userID);

        return view('administratif::pelanggan.profile', ['rs' => $rs, 'counttagihan'=> $counttagihan, 'countbelumbayar'=> $countbelumbayar, 'countterbayar'=> $countterbayar]);
        
    }

    public function edit_profile(Request $request) {

        $username           = Auth::user()->username;

        $rs = User::updateOrCreate(
            ['username' => $username],
            ['nama' => $request->nama, 'alamat' => $request->alamat]
        );

        if ($rs){

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function edit_password(Request $request) {

        $username  = Auth::user()->username;

        $rs = User::updateOrCreate(
            ['username' => $username],
            ['password' => Hash::make($request->password)]
        );

        if ($rs){

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }

    }
}

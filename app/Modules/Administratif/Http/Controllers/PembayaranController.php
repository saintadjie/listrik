<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\tagihan;
use App\Models\penggunaan;
use App\Models\tarif;
use App\Models\pembayaran;
use Yajra\DataTables\DataTables;
use App\User;
use Carbon\Carbon;
use Auth;

class PembayaranController extends Controller
{
    public function pullData(Request $request) {

        
        $level_pengguna = Auth::User()->level_pengguna;
        $username       = Auth::User()->username;

        if($level_pengguna == 1 ){
            return datatables (tagihan::select('id', 'id_tagihan', 'id_penggunaan', 'username', 'bulan', 'tahun', 'jumlah_meter', 'status')
            ->where('deleted_at','=', NULL)
            ->where('STATUS','=', 'BELUM BAYAR')
            ->latest())
            ->addIndexColumn()
            ->toJson();

        } else {
            return datatables (tagihan::select('id', 'id_tagihan', 'id_penggunaan', 'username', 'bulan', 'tahun', 'jumlah_meter', 'status')
            ->where('deleted_at','=', NULL)
            ->where('username', '=', $username)
            ->where('STATUS','=', 'BELUM BAYAR')
            ->latest())
            ->addIndexColumn()
            ->toJson();
        }

    }

    public function index(Request $request) {

        return view('administratif::pembayaran.index');

    }

    public function page_show($id_tagihan) {

        $hari=Carbon::today()->toDateString();

        $rs = tagihan::where('id_tagihan', $id_tagihan)
                ->join('penggunaans', 'tagihans.id_penggunaan', 'penggunaans.id_penggunaan')
                ->join('users', 'penggunaans.username', 'users.username')
                ->join('tarifs', 'users.id_tarif', 'tarifs.id_tarif')
                ->first();


        $rsp = User::select('username', 'nama', DB::raw("CONCAT(username, ' - ', nama) as display"))
            ->where('level_pengguna','!=','1')
            ->get();
        
        return view('administratif::pembayaran.show', ['rs' => $rs, 'rsp' => $rsp, 'hari' => $hari]);

    }


    public function edit(Request $request) {

        $id_tagihan = $request->id_tagihan;
      
        $rs = tagihan::updateOrCreate(
            ['id_tagihan' => $request->id_tagihan],
            ['status' => 'TERBAYAR']
        );

        $rs2 = pembayaran::firstOrCreate(
            ['id_pembayaran' => $request->id_pembayaran],
            ['id_tagihan' => $request->id_tagihan, 'username' => $request->username, 'tanggal_pembayaran' => $request->tanggal_pembayaran, 'biaya_admin' => $request->biaya_admin, 'total_bayar' => $request->total_bayar]
        );


        if ($rs2){

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        } 
    }
}

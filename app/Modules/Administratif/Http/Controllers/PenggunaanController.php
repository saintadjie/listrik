<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\penggunaan;
use App\Models\tagihan;
use Yajra\DataTables\DataTables;
use App\User;
use Auth;

class PenggunaanController extends Controller
{
    public function pullData(Request $request) {

        $level_pengguna = Auth::User()->level_pengguna;
        $username       = Auth::User()->username;

        if($level_pengguna == 1 ){
            return datatables (penggunaan::select('id', 'id_penggunaan', 'username', 'bulan', 'tahun', 'meter_awal', 'meter_akhir')
            ->where('deleted_at','=', NULL)
            ->latest())
            ->addIndexColumn()
            ->toJson();
        } else {
            return datatables (penggunaan::select('id', 'id_penggunaan', 'username', 'bulan', 'tahun', 'meter_awal', 'meter_akhir')
            ->where('deleted_at','=', NULL)
            ->where('username', '=', $username)
            ->latest())
            ->addIndexColumn()
            ->toJson();
        }


    }

    public function index(Request $request) {


        return view('administratif::penggunaan.index');

    }

    public function page_show($id_penggunaan) {

        $rs = penggunaan::where('penggunaans.id_penggunaan', $id_penggunaan)
                ->join('tagihans', 'penggunaans.id_penggunaan', 'tagihans.id_penggunaan')
                ->first();

        $rsp = User::select('username', 'nama', DB::raw("CONCAT(username, ' - ', nama) as display"))
            ->where('level_pengguna','!=','1')
            ->get();


        return view('administratif::penggunaan.show', ['rs' => $rs, 'rsp' => $rsp]);

    }

    public function delete($id_penggunaan) {

        $rs = penggunaan::where('id_penggunaan', $id_penggunaan)->first();
        $rs->delete();

        $rs2 = tagihan::where('id_penggunaan', $id_penggunaan)->first();
        $rs2->delete();

        activity()
        ->performedOn($rs)
        ->withProperties(['id_penggunaan' => $rs->id_penggunaan, 'username' => $rs->username])
        ->log('Menghapus Data ID Penggunaan '. $rs->id_penggunaan);

        if ($rs){

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }
        
    }

    public function page_add() {

        $rs = User::select('username', 'nama', DB::raw("CONCAT(username, ' - ', nama) as display"))
            ->where('level_pengguna','!=','1')
            ->get();

        return view('administratif::penggunaan.add', ['rs' => $rs]);

    }

    public function store(Request $request) {

        $rs = penggunaan::firstOrCreate(
            ['id_penggunaan' => $request->id_penggunaan],
            ['username' => $request->username, 'bulan' => $request->bulan, 'tahun' => $request->tahun, 'meter_awal' => $request->meter_awal, 'meter_akhir' => $request->meter_akhir]
        );


        $meter_awal = $request->meter_awal;
        $meter_akhir = $request->meter_akhir;
        $jumlah_meter = $meter_akhir - $meter_awal;

        $rs2 = tagihan::firstOrCreate(
            ['id_tagihan' => $request->id_tagihan],
            ['id_penggunaan' => $request->id_penggunaan, 'username' => $request->username,'bulan' => $request->bulan, 'tahun' => $request->tahun, 'jumlah_meter' => $jumlah_meter, 'status' => 'BELUM BAYAR']
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['id_penggunaan' => $rs->id_penggunaan, 'username' => $rs->username])
            ->log('Menambah Data ID Penggunaan ' . $rs->id_penggunaan);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function edit(Request $request) {

        $id_penggunaanlama = $request->id_penggunaanlama;
      
        $rs = penggunaan::updateOrCreate(
            ['id_penggunaan'    => $request->id_penggunaan],
            ['username' => $request->username, 'bulan' => $request->bulan, 'tahun' => $request->tahun, 'meter_awal' => $request->meter_awal, 'meter_akhir' => $request->meter_akhir]
        );

        $meter_awal = $request->meter_awal;
        $meter_akhir = $request->meter_akhir;
        $jumlah_meter = $meter_akhir - $meter_awal;

        $rs2 = tagihan::updateOrCreate(
            ['id_tagihan' => $request->id_tagihan],
            ['id_penggunaan' => $request->id_penggunaan, 'username' => $request->username,'bulan' => $request->bulan, 'tahun' => $request->tahun, 'jumlah_meter' => $jumlah_meter]
        );

        if ($rs){

            activity()
            ->performedOn($rs)
            ->withProperties(['id_penggunaan' => $rs->id_penggunaan, 'username' => $rs->username])
            ->log('Mengubah Data ID Penggunaan ' . $id_penggunaanlama);

            return response()->json(['status' => 'OK']);

        } else {

            return response()->json(['status' => 'ERROR']);

        } 
    }
}

<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;
use Hash;
use Auth;

class AlgoritmaController extends Controller
{
    public function index(Request $request) {

        return view('administratif::algoritma.index');

    }
}

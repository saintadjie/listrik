@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datepicker/bootstrap-datepicker.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Pembayaran</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Pembayaran</div>
        <div class="breadcrumb-item active"><a href="#">Ubah Pembayaran</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/pembayaran')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_pembayaran">
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="tanggal_pembayaran">Tanggal Pembayaran</label>
                                <input type="text" value="{{$hari}}" id="tanggal_pembayaran" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="id_pembayaran">Id Pembayaran</label>
                                <input type="text" id="id_pembayaran" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="id_tagihan">Id Tagihan</label>
                                <input type="text"  value="{{$rs->id_tagihan}}" id="id_tagihan" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="id_penggunaan">Id Penggunaan</label>
                                <input type="text"  value="{{$rs->id_penggunaan}}" id="id_penggunaan" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="username">Username</label>
                                <select id="username" class="form-control select2" name="username" disabled>
                                    @foreach($rsp as $result)
                                    <option value="{{$result->username}}" data-usernamename="{{$result->nama}}">{{$result->display}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-md-2">
                                <label for="bulan">Bulan</label>
                                <input type="text" id="bulan" value="{{$rs->bulan}}" class="form-control datepickerm" aria-describedby="bulanHelpBlock" readonly>
                            </div>
                            <div class="form-group col-lg-2 col-md-2">
                                <label for="tahun">Tahun</label>
                                <input type="text" id="tahun" value="{{$rs->tahun}}" class="form-control datepicker" aria-describedby="tahunHelpBlock" readonly>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="jumlah_meter">Jumlah Meter</label>
                                <input type="text" id="jumlah_meter" value="{{$rs->jumlah_meter}}" class="form-control" aria-describedby="jumlah_meterHelpBlock" readonly>
                            </div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="tarifperkwh">Tarif Per KWH</label>
                                <input type="text" id="tarifperkwh" value="{{$rs->tarifperkwh}}" class="form-control" aria-describedby="tarifperkwhHelpBlock" readonly>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="prebayar">Biaya</label>
                                <input type="text" id="prebayar" class="form-control" aria-describedby="prebayarHelpBlock" readonly>
                            </div>
                            <div class="form-group col-lg-2 col-md-2">
                                <label for="biaya_admin">Biaya Admin</label>
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            Rp.
                                        </div>
                                    </div>
                                    <input type="number" id="biaya_admin" class="form-control" autofocus aria-describedby="biaya_adminHelpBlock">                                    
                                </div>
                            </div>
                            <div class="form-group col-lg-2 col-md-2">
                                <label for="total_bayar">Total bayar</label>
                                <input type="text" id="total_bayar" class="form-control" aria-describedby="total_bayarHelpBlock" readonly>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2">
                            </div>
                            <div class="form-group col-lg-8 col-md-8 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>
                    </form>

                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('js/administratif/pembayaran/show_app.js')}}"></script>
    <script src="{{url('out/css/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{url('out/css/select2/select2.full.min.js')}}"></script>
    <script type="text/javascript">
            $('#prebayar').val($('#jumlah_meter').val()*$('#tarifperkwh').val()); 
    </script>

    <script type="text/javascript">
        $("#biaya_admin").on('input', function(){
            $('#total_bayar').val( ((Number(($('#biaya_admin').val())) + Number(($('#prebayar').val())))))
        });

    </script>
 
    <script type="text/javascript">
        var url_api         = "{{url('api/v1/administratif/pembayaran/edit')}}"
        var url_main        = "{{url('/administratif/pembayaran/')}}"
    </script>
@endpush
@endsection
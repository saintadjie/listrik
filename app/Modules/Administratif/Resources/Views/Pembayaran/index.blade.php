@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Pembayaran</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item active"><a href="#">Pembayaran</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                </div>
                <div class="card-body">

                    <div class="table-responsive">
                        <table id="table_form" class="table table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle; text-align: center;">No.</th>
                                    <th style="vertical-align: middle; text-align: center;">ID Tagihan</th>
                                    <th style="vertical-align: middle; text-align: center;">ID Penggunaan</th>
                                    <th style="vertical-align: middle; text-align: center;">Pelanggan</th>
                                    <th style="vertical-align: middle; text-align: center;">Bulan</th>
                                    <th style="vertical-align: middle; text-align: center;">Tahun</th>
                                    <th style="vertical-align: middle; text-align: center;">Jumlah Meter</th>
                                    <th style="vertical-align: middle; text-align: center;">Status</th>
                                    <th style="vertical-align: middle; text-align: center;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>
    


@push('script-footer')
    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('js/administratif/pembayaran/index_app.js')}}"></script>
    <script type="text/javascript">
        var url_main    = "{{url('/administratif/pembayaran/')}}"
        var url_delete  = "{{url('api/v1/administratif/pembayaran/delete')}}"
    </script>
    <script>
        var table_form = $('#table_form').DataTable({
            "language": {
                "emptyTable":     "Tidak ada data yang tersedia",
                "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                "infoFiltered":   "(tersaring dari _MAX_ total data)",
                "lengthMenu":     "Tampilkan _MENU_ data",
                "search":         "Pencarian:",
                "zeroRecords":    "Pencarian tidak ditemukan",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "▶",
                    "previous":   "◀"
                },
            },
            "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
            processing: true,
            serverSide: true,
            ajax: "{{ route ('pullData.pembayaran') }}",
            columns: [
                {   
                    "data": 'DT_RowIndex',
                    "sClass": "text-center",
                    "orderable": false, 
                    "searchable": false
                },
                {
                    "data": "id_tagihan",
                    "sClass": "text-center",
                },
                {
                    "data": "id_penggunaan",
                    "sClass": "text-center",
                },
                {
                    "data": "username",
                    "sClass": "text-center",
                },
                {
                    "data": "bulan",
                    "sClass": "text-center",
                },
                {
                    "data": "tahun",
                    "sClass": "text-center",
                },
                {
                    "data": "jumlah_meter",
                    "sClass": "text-center",
                },
                {
                    "data": "status",
                    "sClass": "text-center",
                },
                {
                    "data": "id_tagihan",
                    "sClass": "text-center",
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {

                        return '@if (auth()->user()->can('Mengubah data pembayaran')) <a href="{{url('/administratif/pembayaran/show')}}/'+data+' " id="btn_edit" class="btn btn-icon icon-left btn-primary"><i class="fas fa-file-invoice-dollar"></i> Bayar</a> @endif @if (auth()->user()->cannot('Mengubah data pembayaran') && ('Menghapus data pembayaran')) <a class="btn btn-icon icon-left btn-danger text-white"><i class="fas fa-times"></i> Tidak memiliki akses</a> @endif';
                    }
                },
            ],
            "createdRow": function( row, data, dataIndex ) {
                $(row).attr('id', data.id_tagihan);
            }
        });
    </script>
@endpush
@endsection
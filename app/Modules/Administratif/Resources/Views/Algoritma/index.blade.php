@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
    <style type="text/css">
        
        #paper {
            color:#FFF;
            font-size:20px;
        }
        #text {
            width:500px;
            overflow:hidden;
            background-color:#FFF;
            color:#222;
            font-family:Courier, monospace;
            font-weight:normal;
            font-size:24px;
            resize:none;
            line-height:40px;
            padding-left:100px;
            padding-right:100px;
            padding-top:45px;
            padding-bottom:34px;
            background-image:url(/images/lines.png), url(/images/paper.png);
            background-repeat:repeat-y, repeat;
            -webkit-border-radius:12px;
            border-radius:12px;
            -webkit-box-shadow: 0px 2px 14px #000;
            box-shadow: 0px 2px 14px #000;
            border-top:1px solid #FFF;
            border-bottom:1px solid #FFF;
        }
    </style>
@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Algoritma</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item active"><a href="#">Algoritma</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                </div>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-3"></div>
                        <div class="form-group col-md-6">
                            <div class="alert alert-primary">
                                Daftar angka yang telah diinput.
                            </div>
                            <textarea id="text" name="nilai" rows="4" style="overflow: hidden; word-wrap: break-word; resize: none; height: 160px; width:100%;" disabled></textarea>
                            <div class=" text-right">
                                <button onclick="sortNilai()" class="btn btn-icon icon-left btn-info"><i class="fas fa-align-justify"></i> Urutkan Data</button>
                                <button onclick="clearNilai()" class="btn btn-icon icon-left btn-danger"><i class="fas fa-times"></i> Bersihkan Data</button>
                            </div>
                        </div>
                        <div class="form-group col-md-3"></div>
                    </div>
                    
                    <div class="form-row" id="satu">
                        <div class="form-group col-md-3"></div>
                        <div class="form-group col-md-6">
                            <div class="badges">
                                <span class="badge badge-primary">1. Input Nilai</span>
                                <span class="badge badge-warning">2. Sorting</span>
                                <span class="badge badge-info">3. Searching</span>
                                <span class="badge badge-danger">4. Clear</span>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input class="form-control" name="perintah" type="number" min="1" max="4" placeholder="Masukan Perintah (1/2/3/4)">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button" onclick="panggilPerintah()">Jalankan Perintah!</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-3"></div>
                    </div>

                    <div class="form-row" id="dua" style="display: none;">
                        <div class="form-group col-md-3"></div>
                        <div class="form-group col-md-6">
                            <div class="alert alert-info">
                                Masukan Nilai!
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input class="form-control" id="input-nilai" name="input-nilai" type="number" min="1" max="999" placeholder="Masukan Nilai">
                                    <div class="input-group-append">
                                        <button class="btn btn-danger" type="button" onclick="kembali()"><i class="fas fa-reply"></i> Kembali!</button>
                                        <button class="btn btn-primary" type="button" onclick="simpanNilai()"><i class="fas fa-save"></i> Simpan Nilai!</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-3"></div>
                    </div>

                    <div class="form-row" id="tiga" style="display: none;">
                        <div class="form-group col-md-3"></div>
                        <div class="form-group col-md-6">
                            <div class="alert alert-info">
                                Cari Nilai!
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input class="form-control" id="cari-nilai" name="cari-nilai" type="number" min="1" max="999" placeholder="Cari Nilai">
                                    <div class="input-group-append">
                                        <button class="btn btn-danger" type="button" onclick="kembali()"><i class="fas fa-reply"></i> Kembali!</button>
                                        <button class="btn btn-primary" type="button" onclick="cariNilaiExe()"><i class="fas fa-search"></i> Cari Nilai!</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-3"></div>
                    </div>

                </div>
            </div>
        </div>         
    </div>
</div>

@push('script-footer')
    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script>
    $(document).ready(function() {
        renderNilai()
    })

    sortNilai = () => {
        let getSessionNilai = sessionStorage.getItem('tempNilai')
        if (getSessionNilai != undefined || getSessionNilai != null || getSessionNilai != '') {
            if (getSessionNilai = JSON.parse(getSessionNilai)){
                getSessionNilai = getSessionNilai.sort(function(a, b){
                    return a-b
                })
                sessionStorage.setItem('tempNilai', JSON.stringify(getSessionNilai))
                renderNilai()
                $('input').val(null)
            } else {
                Swal.fire( "Kesalahan", "Fungsi sorting tidak bisa dilakukan karena belum ada nilai yang disimpan!", "error" )
                return
            }

        }
    }
    kembali = () => {
        $('#input-nilai').val('')
        $('#dua').hide()
        $('#tiga').hide()
        $('[name=perintah]').val(null)
        $('#satu').show()
    }
    clearNilai = () => {
        $('input').val(null)
        sessionStorage.removeItem('tempNilai')
        renderNilai()
        kembali()
    }
    panggilPerintah = () => {
        let perintah = $('[name=perintah]').val()
        
        if (perintah == 1) { 
            $('#satu').hide()
            inputNilai() 
        }
        else if (perintah == 2) { 
            sortNilai()
            $('input').val(null)
            $('#satu').show()
        }
        else if (perintah == 3) {
            $('#satu').hide()
            cariNilai() 
        }
        else if (perintah == 4) { 
            clearNilai() 
        }
        else {
            $('#satu').show()
            Swal.fire( "Kesalahan", "Perintah tidak ada!", "error" )
            return
        }
    }
    inputNilai = () => {
        $('#dua').show()
    }
    simpanNilai = () => {
        let inptNilai = $('[name=input-nilai]').val()
        inptNilai = parseInt(inptNilai)
        if(Math.floor(inptNilai) == inptNilai && $.isNumeric(inptNilai)){
            let getSessionNilai = sessionStorage.getItem('tempNilai')
            if (getSessionNilai == undefined || getSessionNilai == null || getSessionNilai == '') {
                getSessionNilai = []
            }else{
                getSessionNilai = JSON.parse(getSessionNilai)
            }
            getSessionNilai.push(inptNilai)
            sessionStorage.setItem('tempNilai', JSON.stringify(getSessionNilai))
            renderNilai()
        }else{
            Swal.fire( "Kesalahan", "Harap masukan angka!", "error" )
            return
        }
        $('input').val(null)
    }
    renderNilai = () => {
        let getSessionNilai = sessionStorage.getItem('tempNilai')
        if (getSessionNilai != undefined && getSessionNilai != null && getSessionNilai != '') {
            $('[name=nilai]').val(getSessionNilai)
        }else{
            $('[name=nilai]').val(null)
        }
    }
    cariNilai = () => {
        $('#tiga').show()
        renderNilai()
    }
    cariNilaiExe = () => {
        let getSessionNilai = sessionStorage.getItem('tempNilai')
        if (getSessionNilai == undefined || getSessionNilai == null || getSessionNilai == '') {
            Swal.fire( "Kesalahan", "Belum ada nilai yang disimpan!", "error" )
            return
        }else{
            getSessionNilai = JSON.parse(getSessionNilai)
            let cari = $('[name=cari-nilai]').val()
            cari = parseInt(cari)
            cari = getSessionNilai.indexOf(cari)
            if (cari === -1) {
                Swal.fire( "Kesalahan", "Nilai tidak ditemukan!", "error" )
                return
            }else{
                cari += 1
                Swal.fire( "Sukses", "Nilai ditemukan pada urutan ke-"+cari, "success" )
                return
            }
        }
    }
</script>
    
@endpush
@endsection 
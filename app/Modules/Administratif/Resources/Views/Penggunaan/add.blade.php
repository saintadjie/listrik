@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datepicker/bootstrap-datepicker.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Penggunaan</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Penggunaan</div>
        <div class="breadcrumb-item active"><a href="#">Tambah Penggunaan</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/penggunaan')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_penggunaan">
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="id_penggunaan">Id Penggunaan</label>
                                <input type="text" id="id_penggunaan" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="id_tagihan">Id Tagihan</label>
                                <input type="text" id="id_tagihan" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="username">Username</label>
                                <select id="username" class="form-control select2" name="username">
                                    <option value="pilih" disabled selected>Pilih Pelanggan</option>
                                    @foreach($rs as $rs)
                                        <option value="{{$rs->username}}" data-usernamename="{{$rs->nama}}">{{$rs->display}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="bulan">Bulan</label>
                                <input type="text" id="bulan" class="form-control datepickerm" aria-describedby="bulanHelpBlock">
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="tahun">Tahun</label>
                                <input type="text" id="tahun" class="form-control datepicker" aria-describedby="tahunHelpBlock">
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="meter_awal">Meter Awal</label>
                                <input type="number" id="meter_awal" class="form-control" aria-describedby="meter_awalHelpBlock">
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="meter_akhir">Meter Akhir</label>
                                <input type="number" id="meter_akhir" class="form-control" aria-describedby="meter_akhirHelpBlock">
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                                <a class="btn btn-secondary text-white" id="btn_reset">Reset</a>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('out/css/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{url('out/css/select2/select2.full.min.js')}}"></script>
    <script src="{{url('js/administratif/penggunaan/add_app.js')}}"></script>
    <script type="text/javascript">
        $("#meter_akhir").on('blur', function(){
            if (Number($('#meter_akhir').val()) < Number($('#meter_awal').val())) {
                Swal.fire({
                    icon: 'warning',
                    title: 'Kesalahan',
                    text: 'Meter Akhir tidak boleh lebih kecil dari Meter Awal!'
                })
                $('#meter_awal').val('');
                $('#meter_akhir').val('');
                return false;  
            } 
        });

    </script>

    <script type="text/javascript">
        var url_api = "{{url('api/v1/administratif/penggunaan/store')}}"
    </script>
@endpush
@endsection
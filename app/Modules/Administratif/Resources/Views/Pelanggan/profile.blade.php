@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datepicker/bootstrap-datepicker.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>Profile</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item active"><a href="#">Profile</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('/home')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="section-body">
                        <h2 class="section-title">Hi, {{Auth::User()->nama}}!</h2>
                        <p class="section-lead">
                            Kamu bisa mengubah informasi dirimu dihalaman ini.
                        </p>

                        <div class="row mt-sm-4">
                            <div class="col-12 col-md-12 col-lg-4">
                                <div class="card profile-widget">
                                    <div class="profile-widget-header">
                                        @if(Auth::user()->photo != null)
                                            <img alt="image" src="{{url(Auth::user()->photo)}}" class="rounded-circle profile-widget-picture">
                                        @else
                                            <img alt="image" src="{{url('images\user.png')}}" class="rounded-circle profile-widget-picture">
                                        @endif
                                        <div class="profile-widget-items">
                                            <div class="profile-widget-item">
                                                <div class="profile-widget-item-label">Tagihan</div>
                                                <div class="profile-widget-item-value">{{$counttagihan}}</div>
                                            </div>
                                            <div class="profile-widget-item">
                                                <div class="profile-widget-item-label">Belum Bayar</div>
                                                <div class="profile-widget-item-value">{{$countbelumbayar}}</div>
                                            </div>
                                            <div class="profile-widget-item">
                                                <div class="profile-widget-item-label">Terbayar</div>
                                                <div class="profile-widget-item-value">{{$countterbayar}}</div>
                                             </div>
                                        </div>
                                    </div>

                                    <div class="profile-widget-description">
                                        <div class="profile-widget-name">{{Auth::User()->username}} 
                                            <div class="text-muted d-inline font-weight-normal">
                                                <div class="slash"></div> 
                                                {{Auth::User()->nama}}
                                            </div>
                                        </div>
                                        {{Auth::User()->nama}} adalah seorang superhero yang berasal dari <b>Indonesia</b>, Dia bukanlah karakter fiksional, namun dia adalah pahlawan asli bagi keluarganya. Bukan sebuah penghormatan, namun saya hanya bosan dengan nama <b>'John Doe'</b>.
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 col-lg-4">
                                <div class="card">
                                    <form id="form_pelanggan_profile">
                                        <div class="card-header">
                                            <h4>Ubah Profil</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="row" hidden>                               
                                              <div class="form-group col-md-12 col-12">
                                                <label>Username</label>
                                                <input type="text" class="form-control" id="username" value="{{$rs->username}}">
                                              </div>
                                            </div>
                                            <div class="row">                               
                                              <div class="form-group col-md-12 col-12">
                                                <label>Nama</label>
                                                <input type="text" class="form-control" id="nama" value="{{$rs->nama}}">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <div class="form-group col-md-12 col-12">
                                                <label>Alamat</label>
                                                <textarea class="form-control" id="alamat">{{$rs->alamat}}</textarea>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <a class="btn btn-primary text-white mr-1" id="btn_simpan_profile">Simpan</a>
                                        </div>
                                  </form>
                                </div>
                            </div>
                            <div class="col-12 col-md-12 col-lg-4">
                                <div class="card">
                                    <form id="form_pelanggan_password">
                                        <div class="card-header">
                                            <h4>Ubah Password</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">                               
                                              <div class="form-group col-md-12 col-12">
                                                <label>Password</label>
                                                <input type="password" id="password" class="form-control">
                                              </div>
                                            </div>
                                            <div class="row">
                                              <div class="form-group col-md-12 col-12">
                                                <label>Konfirmasi Password</label>
                                                <input type="password" id="konfirmasi_password" class="form-control">
                                              </div>
                                            </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <a class="btn btn-primary text-white mr-1" id="btn_simpan_password">Simpan</a>
                                        </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('js/administratif/pelanggan/profile.js')}}"></script>
    <script src="{{url('out/css/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{url('out/css/select2/select2.full.min.js')}}"></script>
 
    <script type="text/javascript">
        var url_api_profile     = "{{url('api/v1/administratif/pelanggan/edit_profile')}}"
        var url_api_password    = "{{url('api/v1/administratif/pelanggan/edit_password')}}"
        var url_main            = "{{url('/administratif/pelanggan/profile')}}"
    </script>
@endpush
@endsection
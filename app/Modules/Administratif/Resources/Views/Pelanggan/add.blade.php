@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datepicker/bootstrap-datepicker.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Pelanggan</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Pelanggan</div>
        <div class="breadcrumb-item active"><a href="#">Tambah Pelanggan</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/pelanggan')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_pelanggan">
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="username">Username</label>
                                <input type="text" id="username" class="form-control" autofocus>
                            </div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="nama">Nama</label>
                                <input type="text" id="nama" class="form-control">
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="password">Password</label>
                                <input type="password" id="password" class="form-control">
                            </div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="konfirmasi_password">Konfirmasi Password</label>
                                <input type="password" id="konfirmasi_password" class="form-control">
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-8 col-md-8">
                                <label for="alamat">Alamat</label>
                                <textarea type="text" id="alamat" class="form-control"></textarea>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-8 col-md-8">
                                <label for="id_tarif">Tarif Listrik</label>
                                <select id="id_tarif" class="form-control select2" name="id_tarif">
                                    <option value="pilih" disabled selected>Pilih Tarif</option>
                                    @foreach($rs as $rs)
                                        <option value="{{$rs->id_tarif}}" data-id_tarifname="{{$rs->nama}}">{{$rs->display}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2">
                            </div>
                            <div class="form-group col-lg-8 col-md-8 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                                <a class="btn btn-secondary text-white" id="btn_reset">Reset</a>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('out/css/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{url('out/css/select2/select2.full.min.js')}}"></script>
    <script src="{{url('js/administratif/pelanggan/add_app.js')}}"></script>

    <script type="text/javascript">
        var url_api = "{{url('api/v1/administratif/pelanggan/store')}}"
    </script>
@endpush
@endsection